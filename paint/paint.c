// simplelink includes
#include "simplelink.h"

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "interrupt.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "timer.h"
#include "rom.h"
#include "rom_map.h"
#include "pin.h"

// Common interface includes
#include "uart_if.h"
#include "paint.h"
#include "timer_if.h"
#include "common.h"

// Global variables, maybe modified in other functions!
float USER_DEFINE_RADIO = 1.0f;

// File variables
static int cState = 1; //初始状态
static int penState = 1;

// Function declarations
void buzzer();


void TimerServoIntHandler(void)
{
	//Clear the timer interrupt.
	Timer_IF_InterruptClear(TIMERA1_BASE);
	TimerDisable(TIMERA2_BASE, TIMER_B);
	penState=0;
}

void TimerBaseIntHandler(void)
{
    //
    // Clear the timer interrupt.
    //
	Timer_IF_InterruptClear(TIMERA0_BASE);
    cState=0;
}

void SetupTimerServoPWMMode(unsigned long ulBase, unsigned long ulTimer,
                       unsigned long ulConfig, unsigned char ucInvert, unsigned long time)
{
    MAP_TimerConfigure(ulBase,ulConfig);
    MAP_TimerPrescaleSet(ulBase,ulTimer,24);
    MAP_TimerControlLevel(ulBase,ulTimer,ucInvert);
    MAP_TimerLoadSet(ulBase,ulTimer,time);
}

void ServoPWM(int penPosition)
{
	Timer_IF_Init(PRCM_TIMERA1, TIMERA1_BASE, TIMER_CFG_PERIODIC, TIMER_A, 0);
	Timer_IF_IntSetup(TIMERA1_BASE, TIMER_A, TimerServoIntHandler);
	Timer_IF_InterruptClear(TIMERA1_BASE);
	if(penPosition==1)//落笔
	{
		MAP_TimerPrescaleMatchSet(TIMERA2_BASE,TIMER_B,2);//2
		MAP_TimerMatchSet(TIMERA2_BASE,TIMER_B,1500);//28930
		Timer_IF_Start(TIMERA1_BASE, TIMER_A,40);//ms
	}
	else//抬笔
	{
		MAP_TimerPrescaleMatchSet(TIMERA2_BASE,TIMER_B,1);
		MAP_TimerMatchSet(TIMERA2_BASE,TIMER_B,475);//14465//33040*10delay 30900
		Timer_IF_Start(TIMERA1_BASE, TIMER_A,45);//ms

	}
}

void SetupTimerPWMMode(unsigned long ulBase, unsigned long ulTimer,
                       unsigned long ulConfig, unsigned char ucInvert, long long time)
{
    //
    // Set GPT - Configured Timer in PWM mode.
    //
    MAP_TimerConfigure(ulBase,ulConfig);
    MAP_TimerPrescaleSet(ulBase,ulTimer,(unsigned long)time/65535);

    //
    // Inverting the timer output if required
    //
    MAP_TimerControlLevel(ulBase,ulTimer,ucInvert);

    //
    // Load value set to ~0.5 ms time period
    //
    MAP_TimerLoadSet(ulBase,ulTimer,(unsigned long)time-((unsigned long)time/65535)*65535);
    //在不溢出的前提下计算
    //周期=time/(80000000)s
    //频率=80000000/time Hz
    //time=80000000/频率

    //
    // Match value set so as to output level 0
    //
    MAP_TimerPrescaleMatchSet(ulBase,ulTimer,(unsigned long)time/(2*65535));
    MAP_TimerMatchSet(ulBase,ulTimer,(unsigned long)time/2-(unsigned long)time/(2*65535)*65535);

}


void SetYDirection(char dir)
{
	if(dir=='+')
	{
		GPIOPinWrite(GPIOA1_BASE, 0x20, 0x20);//4+
	}
	else
	{
		GPIOPinWrite(GPIOA1_BASE, 0x20, 0);//4-
	}
}

void SetXDirection(char dir)
{
	if(dir=='+')
	{
		GPIOPinWrite(GPIOA1_BASE, 0x10, 0x10);//3+
	}
	else
	{
		GPIOPinWrite(GPIOA1_BASE, 0x10, 0);//3-
	}
}

void PWMProcess(long long ttime, int l_x, int l_y, int l_s, int c_x, int c_y, int c_s)
{


	//舵机操作
	if(c_s!=l_s)
	{
		penState=1;
		MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
		SetupTimerServoPWMMode(TIMERA2_BASE, TIMER_B,
				(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM), 1, 27160);//64 2B
		MAP_TimerEnable(TIMERA2_BASE,TIMER_B);
		ServoPWM(c_s);//1 is down; 0 is up.
		while(1)
		{
			if(penState==0)
			{
				MAP_UtilsDelay(800000*14);//140ms
				break;
			}
		}

	}
	else
	{
//		MAP_UtilsDelay(800000*7);
	}
	buzzer();//蜂鸣器 滴滴
	//MAP_UtilsDelay(800000*14);
	cState=1;
	int xDis = abs(c_x-l_x);
	int yDis = abs(c_y-l_y);
	if(xDis!=0)
	{
		SetupTimerPWMMode(TIMERA3_BASE, TIMER_A,//6.25=100/16
				(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1, ttime*5000/xDis);//2
	}
	else
	{
		SetupTimerPWMMode(TIMERA3_BASE, TIMER_A,//6.25=100/16
						(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1, 0);//2
	}
	//
	// TIMERA3 (TIMER A) as GREEN of RGB light. GPIO 11 --> PWM_7
	//
	if(yDis!=0)
	{
		SetupTimerPWMMode(TIMERA3_BASE, TIMER_B,
				(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1, ttime*5000/yDis);//1
	}
	else
	{
		SetupTimerPWMMode(TIMERA3_BASE, TIMER_B,
						(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1, 0);//1
	}

	MAP_TimerEnable(TIMERA3_BASE,TIMER_A);
	MAP_TimerEnable(TIMERA3_BASE,TIMER_B);

	Timer_IF_Init(PRCM_TIMERA0, TIMERA0_BASE, TIMER_CFG_PERIODIC, TIMER_A, 0);
	Timer_IF_IntSetup(TIMERA0_BASE, TIMER_A, TimerBaseIntHandler);

	Timer_IF_InterruptClear(TIMERA0_BASE);
	Timer_IF_Start(TIMERA0_BASE, TIMER_A,(unsigned long)(USER_DEFINE_RADIO * ttime));//ms

	while(1)
	{
		if(cState==0)
		{
			MAP_TimerDisable(TIMERA3_BASE,TIMER_B);
			MAP_TimerDisable(TIMERA3_BASE,TIMER_A);
			break;
		}
	}


}
//重新初始化PWM信号及计数器
void ReInitPWM()
{
	Timer_IF_DeInit(TIMERA0_BASE, TIMER_A);
	Timer_IF_DeInit(TIMERA1_BASE, TIMER_A);
	/*
	MAP_TimerDisable(TIMERA2_BASE, TIMER_B);
	MAP_TimerDisable(TIMERA3_BASE, TIMER_A);
	MAP_TimerDisable(TIMERA3_BASE, TIMER_B);
	MAP_PRCMPeripheralClkDisable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkDisable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);
	*/
}

void Paint(int l_x, int l_y, int l_s, int c_x, int c_y, int c_s)
{
	if(l_x<0)
	{
		l_x=0;
	}
	if(l_y<0)
	{
		l_y=0;
	}
	if(c_x<0)
	{
		c_x=0;
	}
	if(c_y<0)
	{
		c_y=0;
	}
	int dis=(c_x-l_x)*(c_x-l_x)+(c_y-l_y)*(c_y-l_y);
	UNUSED(dis);	// disable the warning

	int step;
	step = (abs(c_x-l_x)>abs(c_y-l_y))?(abs(c_x-l_x)):(abs(c_y-l_y));
	long long ttime;
	if(step>=50)
	{
		ttime = step * 20;
	}
	else if(step<50&&step>10)
	{
		ttime = step * 50;//时间 100步1s；//单位ms//50
	}
	else
	{
		ttime = step *70;
	}
	//设置方向
	if(c_x > l_x)
	{
		SetXDirection('+');
	}
	else
	{
		SetXDirection('-');
	}
	if(c_y > l_y)
	{
		SetYDirection('+');
	}
	else
	{
		SetYDirection('-');
	}
	//驱动步进电机及舵机程序
	PWMProcess(ttime, l_x, l_y, l_s, c_x, c_y, c_s);
	//重新初始化步进电机及舵机
	ReInitPWM();
}

//蜂鸣器声音 PIN5
void buzzer()
{
		GPIOPinWrite(GPIOA1_BASE, 0x40, 0x40);
		MAP_UtilsDelay(800000);
		GPIOPinWrite(GPIOA1_BASE, 0x40,  0x0);
}

