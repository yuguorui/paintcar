/*
 * paint.h
 *
 *  Created on: 2016年7月21日
 *      Author: yuguorui
 */

#ifndef PAINT_PAINT_H_
#define PAINT_PAINT_H_

// simplelink includes
#include "simplelink.h"

// Global variables
extern float USER_DEFINE_RADIO;


void Paint(int l_x, int l_y, int l_s, int c_x, int c_y, int c_s);
void buzzer();

#endif /* PAINT_PAINT_H_ */
