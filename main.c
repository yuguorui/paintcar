// Standard includes
#include <stdlib.h>
#include <string.h>

// simplelink includes
#include "simplelink.h"
#include "wlan.h"

// driverlib includes
#include "hw_ints.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "uart.h"
#include "utils.h"
#include "gpio.h"

// common interface includes
#include "udma_if.h"
#include "common.h"
#ifndef NOTERM
#include "uart_if.h"
#endif

#include "pinmux.h"

#include "wlan/network.h"
#include "tcp/tcp.h"
#include "paint/paint.h"

#define APP_NAME                "PaintingCar"
#define APPLICATION_VERSION     "1.1.1"

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
#if defined(ccs) || defined (gcc)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************



//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
static void
BoardInit(void)
{
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
  //
  // Set vector table base
  //
#if defined(ccs) || defined (gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif
    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}



//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
static void
DisplayBanner(char * AppName)
{
    Report("\n\n\n\r");
    Report("\t\t *************************************************\n\r");
    Report("\t\t       CC3200 %s Application       \n\r", AppName);
    Report("\t\t *************************************************\n\r");
    Report("\n\n\n\r");
}

void setMS123()
{
	GPIOPinWrite(GPIOA0_BASE, 0x8, 0x8);
	GPIOPinWrite(GPIOA0_BASE, 0x10, 0x10);
	GPIOPinWrite(GPIOA0_BASE, 0x40, 0x40);
}
//*****************************************************************************
//                            MAIN FUNCTION
//*****************************************************************************
void main()
 {
    //
    // Board Initialization
    //
    BoardInit();

    //
    // Initialize the uDMA
    //
    UDMAInit();

    //
    // Configure the pinmux settings for the peripherals exercised
    //
    PinMuxConfig();
    setMS123();

    // startup check
    buzzer();


#ifndef NOTERM
    //
    // Configuring UART
    //
    InitTerm();
#endif

    //
    // Display banner
    //
    DisplayBanner(APP_NAME);

    int i;

/*
    for(i=0;i<10;i++)
{
    	Paint(0,0,0,0,0,1);
 //   	MAP_UtilsDelay(80000000/3);
    	Paint(0,0,1,0,0,0);
 //   	MAP_UtilsDelay(80000000/3);
}
*/

    //
    // Start the WlanAPMode task
    //
    Report("Start the WlanAPMode task.\n\r");
    WlanAPMode(NULL);

    //
    // Start the TCP task
    //
    Report("Start the TCP server.\n\r");
    BsdTcpServer(NULL);

}
