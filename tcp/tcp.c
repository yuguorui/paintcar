// Standard includes
#include <stdlib.h>
#include <string.h>

// simplelink includes 
#include "simplelink.h"
#include "wlan.h"

// driverlib includes 
#include "hw_ints.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "uart.h"
#include "utils.h"

// common interface includes 
#include "udma_if.h"
#include "common.h"
#ifndef NOTERM
#include "uart_if.h"
#endif

#include "../pinmux.h"

#include "../paint/paint.h"

#define PORT_NUM            9999
#define BUF_SIZE            (60000)
#define TCP_PACKET_COUNT    1000

// Application specific status/error codes
typedef enum{
    // Choosing -0x7D0 to avoid overlap w/ host-driver's error codes
    SOCKET_CREATE_ERROR = -0x7D0,
    BIND_ERROR = SOCKET_CREATE_ERROR - 1,
    LISTEN_ERROR = BIND_ERROR -1,
    SOCKET_OPT_ERROR = LISTEN_ERROR -1,
    CONNECT_ERROR = SOCKET_OPT_ERROR -1,
    ACCEPT_ERROR = CONNECT_ERROR - 1,
    SEND_ERROR = ACCEPT_ERROR -1,
    RECV_ERROR = SEND_ERROR -1,
    SOCKET_CLOSE_ERROR = RECV_ERROR -1,
    DEVICE_NOT_IN_STATION_MODE = SOCKET_CLOSE_ERROR - 1,
    STATUS_CODE_MAX = -0xBB8
}e_AppStatusCodes;


//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
int BsdTcpClient(unsigned short usPort, unsigned char* content, unsigned int length);
void BsdTcpServer(void *pvParameters);
void TcpRecvEventHandler(void (*paint)(int, int, int, int, int, int));

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
SlSockAddrIn_t  Addr;
SlSockAddrIn_t  LocalAddr;
_i16 AddrSize = sizeof(SlSockAddrIn_t);
_i16 SockID, newSockID;
_i16 Status;
char Buf[BUF_SIZE];

//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************




//****************************************************************************
//
//! \brief Opening a TCP server side socket and receiving data
//!
//! This function opens a TCP socket in Listen mode and waits for an incoming
//!    TCP connection.
//! If a socket connection is established then the function will try to read
//!    BUF_SIZE bytes from the connected client.
//!
//! \param[in] port number on which the server will be listening on
//!
//! \return     >0 on success, -1 on error.
//!
//! \note   This function will wait for an incoming connection till
//!                     one is established
//
//****************************************************************************
void BsdTcpServer(void *pvParameters)
{
	int port = PORT_NUM;
    LocalAddr.sin_family = SL_AF_INET;
    LocalAddr.sin_port = sl_Htons(port);
    LocalAddr.sin_addr.s_addr = 0;

    Addr.sin_family = SL_AF_INET;
//    Addr.sin_port = sl_Htons(5001);
//    Addr.sin_addr.s_addr = sl_Htonl(SL_IPV4_VAL(10,1,1,200));

    memset(Buf, -1, sizeof(Buf));
    SockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
    Status = sl_Bind(SockID, (SlSockAddr_t *)&LocalAddr, AddrSize);
    Report("Listend to the port: %d. \r\n", port);
    Status = sl_Listen(SockID, 0);
    _i16 iStatus;
    int index = 0;
    Report("Ready to accept connection. \r\n");
    while(1)
    {
    	newSockID = sl_Accept(SockID, (SlSockAddr_t*)&Addr, (SlSocklen_t*) &AddrSize);
		Report("Connection is established.\r\n");
		do
		{
			Status = sl_Recv(newSockID, &Buf[index], 1460, 0);
			index += Status;
			Report("Status: %d\r\n", Status);
		}while(Status != 0);
		Buf[index] = '\0';
		Report("Recieved %u bytes successfully\r\n", index);
		TcpRecvEventHandler(Paint);
		// close the connected socket after receiving from connected TCP client
		iStatus = sl_Close(newSockID);
		memset(Buf, -1, sizeof(Buf));
		index = 0;
//		Report("Socket closed!\r\n");
		UtilsDelay(20);
		UNUSED(iStatus);
    }
}

void TcpRecvEventHandler(void (*paint)(int, int, int, int, int, int))
{
	_i16 c_x, c_y, c_s, l_x = 0, l_y = 0, l_s = 0, tmp;
	char *pEnd = Buf;

	tmp = (_i16)strtol(pEnd, &pEnd, 0);
	USER_DEFINE_RADIO = (float)tmp / 16;
	Report("USER_DEFINE_RADIO: %f \r\n", USER_DEFINE_RADIO);
	while(1)
	{
		c_x = (_i16)strtol(pEnd, &pEnd, 0);
		c_y = (_i16)strtol(pEnd, &pEnd, 0);
		c_s = (_i16)strtol(pEnd, &pEnd, 0);
		if(c_x == -1)
		{
			buzzer();
			break;
		}
		Report("L:%d %d %d C: %d %d %d \r\n", l_x, l_y, l_s, c_x, c_y, c_s);
		paint(l_x, l_y, l_s, c_x, c_y, c_s);
		l_x = c_x;
		l_y = c_y;
		l_s = c_s;
	}
	paint(l_x, l_y, l_s, l_x, l_y, 0);
	Report("Painting is finished!\r\n");
}
